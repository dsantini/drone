#ifndef GYRO_READER_H
#define GYRO_READER_H

#include "structs.h"

//namespace drone{

class gyroReader {
  public:
    virtual struct angoli getAngoli() = 0;
};

//}

#endif // GYRO_READER_H
