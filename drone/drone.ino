#include "structs.h"
#include "config.h"
#include "newMPU9250Reader.h"
#include "oldMPU9250Reader.h"
#include "motor.h"
#include "pid.h"
#include "radioDrone.h"



struct joystick j = defaultJoystick;
struct throttle th = {MIN_THROTTLE, MIN_THROTTLE, MIN_THROTTLE, MIN_THROTTLE, MIN_THROTTLE};
gyroReader* gyro;

void setupGyro(){
  Serial.println("Setup giroscopio...");

  #if GYRO==OLD
    gyro = new oldMPU9250Reader();
  #elif GYRO==NEW
    gyro = new newMPU9250Reader();
  #endif
  }

void setup() {
  setupDebug();
  setupGyro();
  setupRadio();
  setupMotor();
  Serial.println("#########################     Caricamento completato     #########################");
}



void loop() {
  j = leggiRadio(j);

  if (j.calibrazione) {
    calibrazioneESC();
    svuotaRadio();
    j = defaultJoystick;
  }

  struct angoli a  = gyro->getAngoli();
  struct pid p = j.control ? calcoloPID(j, a, th) : defaultPID;
  th = calcoloThrottle(j, p);
  applicaThrottle(th);
  inviaTelemetria(getTelemetria(a, th));

}
