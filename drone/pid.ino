#include "pid.h"
#include "structs.h"
#include "config.h"

float errPitch = 0, errRoll = 0, errYaw = 0;
float sumPitch = 0, sumRoll = 0, sumYaw = 0;

float pid(int vref, int v, int att1, int att2, float p, float i, float d, float *errp, float *o) {
  float der, err; //derivata errore, errore
  float pid;
  err = vref - v;

  der = err - *errp;
  if (att1 < MAX_THROTTLE && att1 > MIN_THROTTLE && att2 < MAX_THROTTLE && att2 > MIN_THROTTLE) *o = *o + err;

  pid = p * err + i * (*o) + d * der;
  *errp = err;

  return pid;
}

struct pid calcoloPID(struct joystick j, struct angoli a, struct throttle t) {
  struct pid p;

  p.roll = ENABLE_ROLL * pid(j.roll, a.roll, t.left, t.right, KP_ROLL, KI_ROLL, KD_ROLL, &errRoll, &sumRoll);
  p.pitch = ENABLE_PITCH * pid(j.pitch, a.pitch, t.front, t.back, KP_PITCH, KI_PITCH, KD_ROLL, &errPitch, &sumPitch);
  p.yaw = ENABLE_YAW * pid(j.yaw, a.yaw, t.left, t.back, KP_YAW, KI_YAW, KD_YAW, &errYaw, &sumYaw);

#ifdef DEBUG_PID
  Serial.print("PID Roll: ");
  Serial.print(p.roll);
  Serial.print("\t Pitch: ");
  Serial.print(p.pitch);
  Serial.print("\t Yaw: ");
  Serial.println(p.yaw);
#endif

  return p;
}

struct throttle calcoloThrottle(struct joystick j, struct pid p) {
  struct throttle t;

  t.up = map(j.throttle, THROTTLE_INVIO_MIN, THROTTLE_INVIO_MAX, MIN_THROTTLE, MAX_THROTTLE);
  t.front = constrain(t.up + p.pitch + p.yaw, MIN_THROTTLE, MAX_THROTTLE);
  t.back = constrain(t.up - p.pitch + p.yaw, MIN_THROTTLE, MAX_THROTTLE);
  t.left = constrain(t.up + p.roll - p.yaw, MIN_THROTTLE, MAX_THROTTLE);
  t.right = constrain(t.up - p.roll - p.yaw, MIN_THROTTLE, MAX_THROTTLE);

  return t;
}
