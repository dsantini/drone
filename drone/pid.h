#ifndef PID_H
#define PID_H



/**
   vref: vel_riferimento
   v: velocità
   att1: vel_mot1
   att2: vel_mot2
   kp
   ki
   kd
    errp: err_prima
    o: sommatoria integrale
*/
float pid(int vref, int v, int att1, int att2, float p, float i, float d, float *errp, float *o);

struct pid calcoloPID(struct joystick j, struct angoli a, struct throttle t);

struct throttle calcoloThrottle(struct joystick j, struct pid p);



#endif
