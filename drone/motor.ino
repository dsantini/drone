#include "motor.h"
#include "structs.h"
#include "config.h"
#include "math.h"

#include <Servo.h>

//Servo motorF, motorB, motorL, motorR;
Servo motorFR, motorFL, motorBR, motorBL;

void setupMotor() {
  Serial.println("Setup motori...");
  //motorF.attach(FRONT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  //motorB.attach(BACK_PIN, MIN_THROTTLE, MAX_THROTTLE);
  //motorL.attach(LEFT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  //motorR.attach(RIGHT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  motorFR.attach(FRONT_RIGHT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  motorFL.attach(FRONT_LEFT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  motorBR.attach(BACK_RIGHT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  motorBL.attach(BACK_LEFT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  applicaThrottle({MIN_THROTTLE, MIN_THROTTLE, MIN_THROTTLE, MIN_THROTTLE, MIN_THROTTLE});
}

int combinaThrottle(int a, int b) {
  double arc = atan2(a, b);
  return a * cos(arc) + b * sin(arc);
}

void applicaThrottle(struct throttle th) {
  stampaThrottle(th);
  //motorF.writeMicroseconds(th.front);
  //motorB.writeMicroseconds(th.back);
  //motorL.writeMicroseconds(th.left);
  //motorR.writeMicroseconds(th.right);
  motorFR.writeMicroseconds(combinaThrottle(th.front, th.right));
  motorFL.writeMicroseconds(combinaThrottle(th.front, th.left));
  motorBR.writeMicroseconds(combinaThrottle(th.back, th.right));
  motorBL.writeMicroseconds(combinaThrottle(th.back, th.left));
}

void calibrazioneESC() {
  Serial.println("#########################     Calibrazione ESC     #########################");

  Serial.println("Invio max throttle");
  applicaThrottle({0, MIN_CALIBRATION_THROTTLE, MIN_CALIBRATION_THROTTLE, MIN_CALIBRATION_THROTTLE, MIN_CALIBRATION_THROTTLE});
  delay(MAX_CALIBRATION_TIME);

  Serial.println("Invio min throttle");
  applicaThrottle({0, MAX_CALIBRATION_THROTTLE, MAX_CALIBRATION_THROTTLE, MAX_CALIBRATION_THROTTLE, MAX_CALIBRATION_THROTTLE});
  delay(MIN_CALIBRATION_TIME);

  applicaThrottle({0, MIN_THROTTLE, MIN_THROTTLE, MIN_THROTTLE, MIN_THROTTLE});

  Serial.println("#########################     Calibrazione ESC completata     #########################");
}
