#ifndef RADIO_H
#define RADIO_H



void setupRadio();

struct joystick leggiRadio(struct joystick defaultJoystick);

void svuotaRadio();

void inviaTelemetria(struct telemetria tel);



#endif
