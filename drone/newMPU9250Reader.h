#ifndef MPU9250_NEW_READER_H
#define MPU9250_NEW_READER_H

#include "gyroReader.h"

#include <MPU9250.h> // https://github.com/sparkfun/SparkFun_MPU-9250_Breakout_Arduino_Library


class newMPU9250Reader : public gyroReader {
  private:
    MPU9250 myIMU;

  public:
    newMPU9250Reader();
    struct angoli getAngoli();
};

#endif // MPU9250_NEW_READER_H
