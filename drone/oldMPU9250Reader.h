#ifndef MPU9250_OLD_READER_H
#define MPU9250_OLD_READER_H

#include "gyroReader.h"


class oldMPU9250Reader : public gyroReader {
  private:
    float FunctionsPitchRoll(double A, double B, double C);

  public:
    oldMPU9250Reader();
    struct angoli getAngoli();
};

#endif // MPU9250_OLD_READER_H
