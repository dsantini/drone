#ifndef CONFIG_H
#define CONFIG_H

/****************************** CONFIGURAZIONE TELECOMANDO ******************************/

// DEBUG TELECOMANDO: commentare i debug a cui non si è interessati
#define DEBUG_JOYSTICK
#define DEBUG_TELEMETRIA
//#define DEBUG_SETUP
//#define DEBUG_TEMPO

//#define TELEMETRIA_RAW
#define SEPARATORE ','

// PIN di connessione ai joystick
#define ROLL_PIN A3 // Js destro orizzontale
#define PITCH_PIN A2 // Js destro verticale
#define RB_PIN 2 // Pulsante destro
#define YAW_PIN A5 // Js sinistro orizzontale
#define THROTTLE_PIN A4 // Js sinistro verticale
#define LB_PIN 5 // Pulsante sinistro

// Valori di ingresso min e max dai joystick
#define THROTTLE_JS_MIN 512
#define THROTTLE_JS_MAX 0
#define PITCH_JS_MIN 0
#define PITCH_JS_MAX 1024
#define ROLL_JS_MIN 1024
#define ROLL_JS_MAX 0
#define YAW_JS_MIN 1024
#define YAW_JS_MAX 0

// Valori min e max inviati al drone
#define THROTTLE_INVIO_MIN 0
#define THROTTLE_INVIO_MAX 1000
#define ANGOLI_INVIO_MIN -30
#define ANGOLI_INVIO_MAX 30

/****************************** CONFIGURAZIONE DRONE ******************************/

// DEBUG DRONE: scommentare i debug desiderati, commentare quelli non desiderati
//#define DEBUG_MOTORI
//#define DEBUG_RICEZIONE
//#define DEBUG_GYRO
//#define DEBUG_PID
#define INVIO_TELEMETRIA

// PIN di controllo agli ESC !!!_OBSOLETI_!!!
#define FRONT_PIN 11
#define BACK_PIN 10
#define LEFT_PIN 9
#define RIGHT_PIN 6

// PIN di controllo agli ESC
#define FRONT_RIGHT_PIN 11 //10
#define FRONT_LEFT_PIN 10 //6
#define BACK_RIGHT_PIN 9 //11
#define BACK_LEFT_PIN 6 // 9

// Lettura giroscopio
// GYRO: Giroscopio utilizzato (NEW=codice Sparkfun, OLD=codice Alex)
#define GYRO OLD

// COSTANTI PID
#define KP 8
#define KI 0
#define KD 1

#define KP_ROLL KP
#define KI_ROLL KI
#define KD_ROLL KD
#define KP_PITCH KP
#define KI_PITCH KI
#define KD_PITCH KD
#define KP_YAW KP
#define KI_YAW KI
#define KD_YAW KD

// PID abilitati
#define ENABLE_ROLL 1
#define ENABLE_PITCH 1
#define ENABLE_YAW 0

// THROTTLE MAX e MIN valori di throttle ammessi in uscita agli ESC
//#define MAX_THROTTLE 1950
#define MAX_THROTTLE 2000
#define MIN_THROTTLE 1050

// THROTTLE DI CALIBRAZIONE
#define MAX_CALIBRATION_THROTTLE 2000 // Throttle massimo
#define MIN_CALIBRATION_THROTTLE 1000 // Throttle minimo

// TEMPI DI CALIBRAZIONE
#define MAX_CALIBRATION_TIME 13000 // Tempo per il throttle massimo, in millisecondi
#define MIN_CALIBRATION_TIME 2500 // Tempo per il throttle minimo, in millisecondi

// RADIO
// RADIO TIMEOUT: Dopo quanto considerare la radio disconnessa, in millisecondi
#define RADIO_TIMEOUT 5000
// INTERVALLO_TELEMETRIA: Ogni quanto inviare la telemetria, in millisecondi
#define INTERVALLO_TELEMETRIA 30

// Declinazione magnetica
// http://www.magnetic-declination.com/ITALY/IMOLA/1315479.html
// Imola: 44° 21' 50.4" N     11° 41' 32.5" E     Magnetic Declination: +3° 7' -> 3+7/60=187/60=3.117°
#define DECLINATION 3.117



#endif
