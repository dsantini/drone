#include <cstdio>
#include <cstring>
#include <SDL2/SDL.h>
#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <chrono>
#include <thread>

bool continua = true;

void interruptHandler(int x){
	continua = false;
}

short getAxis(SDL_GameController* controller, SDL_GameControllerAxis axis){
	return SDL_GameControllerGetAxis(controller, axis) / 327; // [-32768,32767] -> [-100,100] oppure [0,32767] -> [0,100]
}

bool getButton(SDL_GameController* controller, SDL_GameControllerButton button){
	return (bool)SDL_GameControllerGetButton(controller, button); // {0,1} -> {true,false}
}

int usaController(int joystick_index){
	printf("Caricamento di %s\n", SDL_GameControllerNameForIndex(joystick_index));
	SDL_GameController* controller = SDL_GameControllerOpen(joystick_index);
	if(controller == NULL){
		puts("Errore nel caricamento del controller");
		return -1;
	}

	while(continua){
		SDL_GameControllerUpdate();
		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		short lx = getAxis(controller, SDL_CONTROLLER_AXIS_LEFTX),
			rx = getAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX),
			ly = -getAxis(controller, SDL_CONTROLLER_AXIS_LEFTY),
			ry = -getAxis(controller, SDL_CONTROLLER_AXIS_RIGHTY),
			lt = getAxis(controller, SDL_CONTROLLER_AXIS_TRIGGERLEFT),
			rt = getAxis(controller, SDL_CONTROLLER_AXIS_TRIGGERRIGHT);

		bool lb = getButton(controller, SDL_CONTROLLER_BUTTON_LEFTSHOULDER),
			rb = getButton(controller, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER);

		printf("lx %hd ly %hd rx %hd ry %hd lt %hd rt %hd lb %d rb %d\n", lx, ly, rx, ry, lt, rt, lb, rb);
		
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}

	puts("Terminazione");
	SDL_GameControllerClose(controller);
	return 0;
}

int main(){
	signal(SIGINT, interruptHandler);

	int init = SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER);
	if(init < 0){
		printf("Errore nell'inizializzazione: %s", SDL_GetError());
		return -1;
	}
	atexit(SDL_Quit);

	int mappings = SDL_GameControllerAddMappingsFromFile("gamecontrollerdb.txt");
	if(mappings < 1){
		puts("Errore nel caricamento mappature da file");
		return -1;
	}

	int numJoysticks = SDL_NumJoysticks();
	if(numJoysticks < 0){
		printf("Errore nella rilevazione dei joystick: %s", SDL_GetError());
		return -1;
	} else if(numJoysticks == 0){
		puts("Nessun controller rilevato");
		return -1;
	}

	int joystick_index = -1; 
	for (int i = 0; i < numJoysticks; i++) {
		const char* nome = SDL_GameControllerNameForIndex(i);
    if ( ! SDL_IsGameController(i)){
       printf("Joystick %i (%s) non supportato\n", i, nome);
		} else {
       printf("Joystick %i (%s) supportato\n", i, nome);
			 joystick_index = i;
    }
	}
	if(joystick_index < 0){
		puts("Nessun controller supportato");
		return -1;
	}

	return usaController(joystick_index);
}