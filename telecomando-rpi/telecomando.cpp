#include <iostream>
#include <RF24/RF24.h>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <ctime>
#include <csignal>
#include <SDL2/SDL.h>
#include "../drone/structs.h"
#include "../drone/config.h"

RF24 radio(RPI_V2_GPIO_P1_22, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_8MHZ);
std::fstream fout;
bool continua = true;
SDL_GameController* controller;

/*************** JOYSTICK ***************/

void setupJoystick(){
	std::cout << "Inizio setup joystick" << std::endl;

	int init = SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER);
	if(init < 0){
		std::cout << "Errore nell'inizializzazione: " << SDL_GetError() << std::endl;
		exit(-1);
	}
	atexit(SDL_Quit);

	int mappings = SDL_GameControllerAddMappingsFromFile("gamecontrollerdb.txt");
	if(mappings < 1){
		std::cout << "Errore nel caricamento mappature da file" << std::endl;
		exit(-1);
	}

	int numJoysticks = SDL_NumJoysticks();
	if(numJoysticks < 0){
		std::cout << "Errore nella rilevazione dei joystick: %s" << SDL_GetError() << std::endl;
		exit(-1);
	} else if(numJoysticks == 0){
		std::cout << "Nessun controller rilevato" << std::endl;
		exit(-1);
	}

	int joystick_index = -1; 
	for (int i = 0; i < numJoysticks; i++) {
		const char* nome = SDL_GameControllerNameForIndex(i);
    if ( ! SDL_IsGameController(i)){
      	std::cout << "Joystick " << i << " (" << nome << ") non supportato" << std::endl;
		} else {
   		std::cout << "Joystick " << i << " (" << nome << ") supportato" << std::endl;
			joystick_index = i;
    }
	}
	if(joystick_index < 0){
		std::cout << "Nessun controller supportato" << std::endl;
		exit(-1);
	}

	std::cout << "Caricamento di " << SDL_GameControllerNameForIndex(joystick_index) << std::endl;
	SDL_GameController* controller = SDL_GameControllerOpen(joystick_index);
	if(controller == NULL){
		std::cout << "Errore nel caricamento del controller" << std::endl;
		exit(-1);
	}

	std::cout << "Fine setup joystick" << std::endl;
}

float map(float x, float x1, float x2, float y1, float y2)
{
 return (x - x1) * (y2 - y1) / (x2 - x1) + y1;
}

/**
[-32768,32767] o [0,32767]
*/
short getAxis(SDL_GameController* controller, SDL_GameControllerAxis axis){
	return SDL_GameControllerGetAxis(controller, axis);
}

/**
[-max,max]
*/
short getScaledAxis(SDL_GameController* controller, SDL_GameControllerAxis axis, short max){
	return SDL_GameControllerGetAxis(controller, axis) / 32767 * max;
}

/**
{true,false}
*/
bool getButton(SDL_GameController* controller, SDL_GameControllerButton button){
	return (bool)SDL_GameControllerGetButton(controller, button);
}

struct joystick leggiJoystick(){
	struct joystick j = defaultJoystick;
	SDL_GameControllerUpdate();
	std::cout << getAxis(controller, SDL_CONTROLLER_AXIS_RIGHTY) << " " << getScaledAxis(controller, SDL_CONTROLLER_AXIS_RIGHTY, THROTTLE_INVIO_MAX) << std:: endl;

	bool lb = getButton(controller, SDL_CONTROLLER_BUTTON_LEFTSHOULDER),
		rb = getButton(controller, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER);
	
	j.calibrazione = rb && lb;

	if( ! j.calibrazione){
		j.throttle = - getScaledAxis(controller, SDL_CONTROLLER_AXIS_RIGHTY, THROTTLE_INVIO_MAX);
		j.control = ! rb;
		if(j.control){
			j.pitch = getScaledAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX, ANGOLI_INVIO_MAX);
			j.roll = - getScaledAxis(controller, SDL_CONTROLLER_AXIS_LEFTY, ANGOLI_INVIO_MAX);
			j.yaw = getScaledAxis(controller, SDL_CONTROLLER_AXIS_LEFTX, ANGOLI_INVIO_MAX);
		}
	}

	return j;
}

void inviaJoystick(struct joystick j) {
	radio.stopListening();
	radio.write(&j, sizeof(j));
	radio.startListening();
}

/*************** TELEMETRIA ***************/

void setupRadio(){
	std::cout << "Inizio setup radio" << std::endl;
	radio.begin();
	radio.setRetries(15, 15);
	radio.setPALevel(RF24_PA_MAX);
	radio.openWritingPipe(0xF0F0F0F0A1);
	radio.openReadingPipe(1, 0xF0F0F0F0A2);
	radio.startListening();
	std::cout << "Fine setup radio" << std::endl;
}

bool telemetriaDisponibile(){
	return radio.available() && (radio.getDynamicPayloadSize() >= sizeof(struct telemetria));
}

struct telemetria riceviTelemetria(){
	struct telemetria t;

	radio.read((void*)&t, sizeof(struct telemetria));

	return t;
}

/*************** CSV FILE ***************/

long int getTimestamp(){
	return static_cast<long int>(std::time(0));
}

void setupFile(){
	std::cout << "Inizio setup file" << std::endl;
	char titolo[25];
	snprintf(titolo, 25, "drone%ld.csv", getTimestamp());
	fout.open(titolo, std::ios::out | std::ios::app);
	fout << "Pitch,Roll,Front,Right,Back,Left,jsCalibrazione,jsControl,jsThrottle,jsPitch,jsRoll,jsYaw\n";
	std::cout << "Fine setup file: " << titolo << std::endl;
}

void stampaCSV(struct telemetria t, struct joystick j){
	//std::cout << "Telemetria: " <<  t.pitch << "\t" << t.roll << std::endl;
	fout << t.pitch << ','
		<< t.roll << ','
		<< t.front << ','
		<< t.right << ','
		<< t.back << ','
		<< t.left << ','
		<< j.calibrazione << ','
		<< j.control << ','
		<< j.throttle << ','
		<< j.pitch << ','
		<< j.roll << ','
		<< j.yaw << '\n';
}

/*************** MAIN ***************/

void interruptHandler(int x){
	continua = false;
	(void)x;
}

void setup() {
	std::cout << "Inizio setup" << std::endl;
	signal(SIGINT, interruptHandler);
	setupFile();
	setupRadio();
	setupJoystick();
	std::cout << "Fine setup" << std::endl;
}

void teardown(){
	std::cout << "Inizio teardown" << std::endl;
	SDL_GameControllerClose(controller);
	fout.close();
	std::cout << "Fine teardown" << std::endl;
}

int main() {
	setup();

	while (continua) {

		if (telemetriaDisponibile()) {
			struct telemetria t = riceviTelemetria();
			struct joystick j = leggiJoystick();

			inviaJoystick(j);
			stampaCSV(t, j);

			std::cout << '.' << std::flush;
		}

	}

	teardown();
}
