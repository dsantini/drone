#include "radioTelecomando.h"
#include "joystick.h"
#include "config.h"
#include "structs.h"
#include "debugTelecomando.h"

void setup() {
  setupDebug();
  setupJoystick();
  setupRadio();
}

void loop() {
  struct joystick j = leggiJoystick();
  durata("leggi");

  stampaJoystick(j);
  inviaJoystick(j);
  durata("invia");

  struct telemetria t = riceviTelemetria();
  stampaTelemetria(t);
  durata("ricevi");
}
