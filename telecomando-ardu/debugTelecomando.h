#ifndef DEBUG_TELECOMANDO
#define DEBUG_TELECOMANDO



void setupDebug();

void stampaJoystick(struct joystick j);

void stampaTelemetria(struct telemetria t);

void durata(const char* nome);



#endif