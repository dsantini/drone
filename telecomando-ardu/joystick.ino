#include "joystick.h"
#include "structs.h"
#include "config.h"
#include "debugTelecomando.h"

void setupJoystick() {
#ifdef DEBUG_SETUP
  Serial.println("Setup Joystick...");
#endif

  pinMode(RB_PIN, INPUT);
  digitalWrite(RB_PIN, HIGH);
  pinMode(LB_PIN, INPUT);
  digitalWrite(LB_PIN, HIGH);

#ifdef DEBUG_SETUP
  Serial.println("Setup Joystick completato");
#endif
}

struct joystick leggiJoystick() {
  joystick j = defaultJoystick;

  const bool rb = !digitalRead(RB_PIN), lb = !digitalRead(LB_PIN);
  if (rb && lb)
    j.calibrazione = true;
  else {
    j.throttle = map(analogRead(THROTTLE_PIN), THROTTLE_JS_MIN, THROTTLE_JS_MAX, THROTTLE_INVIO_MIN, THROTTLE_INVIO_MAX);
    if (rb)
      j.control = false;
    else {
      j.pitch = map(analogRead(PITCH_PIN), PITCH_JS_MIN, PITCH_JS_MAX, ANGOLI_INVIO_MIN, ANGOLI_INVIO_MAX);
      j.roll = map(analogRead(ROLL_PIN), ROLL_JS_MIN, ROLL_JS_MAX, ANGOLI_INVIO_MIN, ANGOLI_INVIO_MAX);
      j.yaw = map(analogRead(YAW_PIN), YAW_JS_MIN, YAW_JS_MAX, ANGOLI_INVIO_MIN, ANGOLI_INVIO_MAX);
    }
  }

  return j;
}
