#include "radioTelecomando.h"
#include "structs.h"
#include "config.h"

#include <RF24.h> // https://github.com/nRF24/RF24
#include <RF24_config.h>
#include <stdio.h>

RF24 radio(7, 8); // CE, CSN
struct telemetria t = defaultTelemetria;

void setupRadio() {
#ifdef DEBUG_SETUP
  Serial.println("Setup radio...");
#endif

  radio.begin();
  radio.openWritingPipe(0xF0F0F0F0A1);
  radio.openReadingPipe(1, 0xF0F0F0F0A2);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();

#ifdef DEBUG_SETUP
  Serial.println("Setup radio completato");
  Serial.println(radio.isChipConnected() ? "Radio connesso" : "Radio non connesso");
#endif
}

void inviaJoystick(struct joystick j) {
  radio.stopListening();
  radio.write(&j, sizeof(j));
  radio.startListening();
}

struct telemetria riceviTelemetria() {
  if (radio.available()) {
    radio.read(&t, sizeof(t));
  }
  return t;
}
