#include <Servo.h>
#include<Wire.h>
#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
//#include <RF24_config.h>

#define kp_roll 0.8
#define ki_roll 0.4
#define kd_roll 0.3
#define kp_pitch 0.8
#define ki_pitch 0.4
#define kd_pitch 0.3
#define kp_yaw 1.2
#define ki_yaw 0.3
#define kd_yaw 0.3
#define attmax 2000
#define attmin 1070

Servo motorFR, motorBK, motorL, motorR;
int throttle=1200;
const int MPU=0x68;  // I2C address of the MPU-6050
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
float speedFR,speedBK, speedL, speedR;
double fun;
unsigned long int t,t2;
char go;
int i;
int en[3]={1,1,0};
float vmax,v,vrf_roll,vrf_pitch,vrf_yaw;
float pidval[3],e[4],o[4];

RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";

void setup() {
  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);

  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN + (RF24_PA_MAX-RF24_PA_MIN)*2/3);
  radio.startListening();
  
  motorFR.attach(11,1000,2000);
  motorBK.attach(10,1000,2000);
  motorL.attach(9,1000,2000);
  motorR.attach(6,1000,2000);
  motorFR.writeMicroseconds(1000);
  motorBK.writeMicroseconds(1000);
  motorL.writeMicroseconds(1000);
  motorR.writeMicroseconds(1000);
  t=millis();
for(i=0;i<4;i++) e[i]=0;
for(i=0;i<4;i++) o[i]=0;
vrf_roll=0;
vrf_pitch=0;
vrf_yaw=0;
delay(100);

}

void loop() {
  go=Serial.read();
  
  if(go==49){ //go==1 '1'
    t2=millis();
    do{
       Wire.beginTransmission(MPU);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,14,true);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  t=millis()-t2;
  fun=f_yaw(0.5*t,1);
  pidval[0]=en[0]*pid(vrf_roll,FunctionsPitchRoll(AcX,AcY,AcZ),speedL,speedR,kp_roll,ki_roll,kd_roll,&e[0],&o[0]);
  pidval[1]=en[1]*pid(vrf_pitch,FunctionsPitchRoll(AcY,AcX,AcZ),speedFR,speedBK,kp_pitch,ki_pitch,kd_pitch,&e[1],&o[1]);
  pidval[2]=en[2]*pid(vrf_yaw,FunctionsPitchRoll(AcX,AcY,AcZ),speedL,speedBK,kp_yaw,ki_yaw,kd_yaw,&e[2],&o[2]);
  speedFR=throttle+pidval[1]+pidval[2];
  speedBK=throttle-pidval[1]+pidval[2];
  speedL=throttle+pidval[0]-pidval[2];
  speedR=throttle-pidval[0]-pidval[2];
  if(speedFR>attmax) speedFR=attmax;
  if(speedFR<attmin) speedFR=attmin;
  if(speedBK>attmax) speedBK=attmax;
  if(speedBK<attmin) speedBK=attmin;
  if(speedL>attmax) speedL=attmax;
  if(speedL<attmin) speedL=attmin;
  if(speedR>attmax) speedR=attmax;
  if(speedR<attmin) speedR=attmin;
  motorFR.writeMicroseconds(speedFR);
  motorBK.writeMicroseconds(speedBK);
  motorL.writeMicroseconds(speedL);
  motorR.writeMicroseconds(speedR);

 /* t=millis()-t2;
  fun=f_yaw(0.3*t,1);
  speed=throttle+10*fun;
  
  motor.writeMicroseconds(speed);
  Serial.print(fun);
  Serial.print("  ");
  Serial.print(t);
  Serial.print("  ");*/
  Serial.println(speedL);
  delay(50);
      go=Serial.read();
      if(go==48){
           motorFR.writeMicroseconds(1000);
           motorBK.writeMicroseconds(1000);
           motorL.writeMicroseconds(1000);
           motorR.writeMicroseconds(1000);
         for(i=0;i<4;i++) o[i]=0;
         for(i=0;i<4;i++) e[i]=0;
        }
      }while(go!=48);
      
    }

}

