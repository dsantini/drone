#include<Wire.h>

float pid(int vref, int v, int att1, int att2, float p, float i, float d, float *errp, float *o){ //vel_riferimento, velocità, vel_mot1, vel_mot2, kp, ki, kd, err_prima, sommatoria integrale
  float der,err; //derivata errore, errore
  float pid;
  err=vref-v;

  der=err-*errp;
  if(att1<attmax && att1>attmin && att2<attmax && att2>attmin) *o=*o+err;

  pid=p*err+i*(*o)+d*der;
  *errp=err;
  

  return (int)pid;
  }


void controlmot(float pid, float *motspeed){
  *motspeed=*motspeed+pid;
  motorL.writeMicroseconds(int(*motspeed));
  }
