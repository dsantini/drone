#define Dthrot 2
#define Dpitch 2
#define Droll 2
#define tlim_throttle 1000
#define tlim_roll 1000
#define tlim_pitch 1000

void UpDw(int *throttle, unsigned long int t){ //throttle, tempo
  if(t<=tlim_throttle) *throttle+=Dthrot*t;
  }

void TurnLR(int *roll, int roll_i, unsigned long int t){ //roll, roll iniziale, tempo
  if(t<=tlim_roll) *roll+=Droll*t;
  if(t>=tlim_roll && *roll>roll_i) *roll-=Droll*t;
  }

void TurnFB(int *pitch, int pitch_i, unsigned long int t){ //pitch, pitch iniziale, tempo
  if(t<=tlim_pitch) *pitch+=Dpitch*t;
  if(t>=tlim_pitch && *pitch>pitch_i) *pitch-=Dpitch*t;
  }

void Sequenza(int *throttle, int *roll, int *pitch, int roll_i, int pitch_i, unsigned long int t, int en_seq[3]){
  int tr=*throttle;
  int rl=*roll;
  int pi=*pitch;
  if(en_seq[0]==1)UpDw(&tr,t);
  if(en_seq[1]==1)TurnLR(&rl,roll_i,t);
  if(en_seq[2]==1)TurnFB(&pi,pitch_i,t);
  *throttle=tr;
  *roll=rl;
  *pitch=pi;
  }


float f_yaw(int x, int f){
  float yrf;
  int s=1000;
  if(f==0){
    if(x<s) yrf=x*30;
    if(x>=s && x<2*s) yrf=(2*s-x)*30;
    if(x>=2*s) yrf=0;
    }

  if(f==1){
    if(x<1500) yrf=30000;
    if(x>=1500 && x<3000) yrf=-40000;
    if(x>=3000) yrf=0;
    }

  return yrf/1000;
  }

void f_yaw2(int x, int f, double *yrf){
  int s=1000;
  if(f==0){
    if(x<s) *yrf=x*30;
    if(x>=s && x<2*s) *yrf=*yrf-x*30;
    if(x>=2*s) *yrf=0;
    }

  if(f==1){
    if(x<1500) *yrf=30000;
    if(x>=1500 && x<3000) *yrf=30000-(20*x-1500);
    if(x>=3000) *yrf=0;
    }
*yrf=*yrf/1000;
//  return yrf/1000;
  }
