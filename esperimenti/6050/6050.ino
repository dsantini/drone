#include <Wire.h>
#include <math.h>

/***** CONFIGURAZIONE *****/
#define DELAY 100 //millisecondi

#define MEMORIA 10 // profondità memoria


/***** NIZIO CODICE *****/
#define MPU 0x68
#define TOTALE (MEMORIA*(MEMORIA+1)/2)
float pitch[MEMORIA], pitchOffset, pitchValue, roll[MEMORIA], rollOffset, rollValue;

void setup()
{
  Serial.begin(9600);
  Serial.println("\n###############     Caricamento in corso...     ###############");

  Wire.begin(); // join i2c bus (address optional for master)
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);

  Serial.println("Calcolo offset");

  for (int i = 0; i < MEMORIA; i++)
    rilevazione();
  calcolaMedia();
  pitchOffset = -pitchValue;
  rollOffset = -rollValue;

  Serial.println("###############     Caricamento completato     ###############");
}



void loop()
{
  rilevazione();
  calcolaMedia();

  Serial.print("GYRO Pitch: ");
  Serial.print(pitchValue);
  Serial.print("\t Roll: ");
  Serial.println(rollValue);

  delay(DELAY);
}

void calcolaMedia() {
  float sumPitch = 0, sumRoll = 0;

  for (int i = 0; i < MEMORIA; i++) {
    sumPitch += pitch[i] * (i + 1);
    sumRoll += roll[i] * (i + 1);
  }

  pitchValue = sumPitch / TOTALE;
  rollValue = sumRoll / TOTALE;
}

void rilevazione() {
  int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;

  for (int i = 0; i < MEMORIA - 1; i++) {
    pitch[i] = pitch[i + 1];
    roll[i] = roll[i + 1];
  }

  Wire.beginTransmission(MPU);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 14, true); // request a total of 14 registers

  AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp = Wire.read() << 8 | Wire.read(); // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX = Wire.read() << 8 | Wire.read(); // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY = Wire.read() << 8 | Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ = Wire.read() << 8 | Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  //pitch[MEMORIA - 1] = FunctionsPitchRoll(AcY, AcX, AcZ) + pitchOffset;
  //roll[MEMORIA - 1] = FunctionsPitchRoll(AcX, AcY, AcZ) + rollOffset;
  pitch[MEMORIA - 1] = FunctionsPitchRoll(-AcY, AcX, AcZ) + pitchOffset;
  roll[MEMORIA - 1] = FunctionsPitchRoll(AcX, AcY, AcZ) + rollOffset;
}

float FunctionsPitchRoll(double A, double B, double C) {
  float DatoA, DatoB, Value;
  DatoA = A;
  DatoB = (B * B) + (C * C);
  DatoB = sqrt(DatoB);

  Value = atan2(DatoA, DatoB);
  Value = Value * 180 / M_PI;

  return Value;
}
