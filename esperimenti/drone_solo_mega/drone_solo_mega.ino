#include <Servo.h>
#include<Wire.h>
#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

#define kp_roll 6
#define ki_roll 0
#define kd_roll 1
#define kp_pitch 6
#define ki_pitch 0
#define kd_pitch 1
#define kp_yaw 0
#define ki_yaw 0
#define kd_yaw 0
#define attmax 1930
#define attmin 1070
#define MAX_THROTTLE 1930
#define MIN_THROTTLE 1070

Servo motorFR, motorBK, motorL, motorR;
int throttle = 1200;
const int MPU = 0x68; // I2C address of the MPU-6050
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;
float speedFR, speedBK, speedL, speedR;
double fun;
unsigned long int t, t2;
char go;
int i;
int en[3] = {1, 1, 0};
float vmax, v, vrf_roll, vrf_pitch, vrf_yaw;
float pidval[3], e[3], o[3];
float oo;

RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";

void setup() {
  Serial.begin(9600);

  Serial.println("I2C begin...");
  Wire.begin();
  Serial.println("I2C beginTransmission...");
  Wire.beginTransmission(MPU);
  Serial.println("I2C PWR_MGMT_1...");
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Serial.println("I2C wake up...");
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Serial.println("I2C endTransmission...");
  Wire.endTransmission(true);

  Serial.println("Radio...");
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN + (RF24_PA_MAX - RF24_PA_MIN) * 2 / 3);
  radio.startListening();

  Serial.println("ESC...");
  motorFR.attach(11, 1000, 2000);
  motorBK.attach(10, 1000, 2000);
  motorL.attach(9, 1000, 2000);
  motorR.attach(8, 1000, 2000);
  motorFR.writeMicroseconds(1000);
  motorBK.writeMicroseconds(1000);
  motorL.writeMicroseconds(1000);
  motorR.writeMicroseconds(1000);


  Serial.println("PID...");
  t = millis();
  for (i = 0; i < 3; i++) e[i] = 0;
  for (i = 0; i < 3; i++) o[i] = 0;
  for (i = 0; i < 3; i++) pidval[i] = 0;
  oo = 0;
  vrf_roll = 0;
  vrf_pitch = 0;
  vrf_yaw = 0;

  delay(100);
}

void loop() {
  go = Serial.read();
  if (go == 49) {
    t2 = millis();
    do {
      Wire.beginTransmission(MPU);
      Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
      Wire.endTransmission(false);
      Wire.requestFrom(MPU, 14, true); // request a total of 14 registers
      AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
      AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
      AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
      Tmp = Wire.read() << 8 | Wire.read(); // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
      GyX = Wire.read() << 8 | Wire.read(); // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
      GyY = Wire.read() << 8 | Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
      GyZ = Wire.read() << 8 | Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

      t = millis() - t2;
      // fun = f_yaw(0.5 * t, 1);
      pidval[0] = en[0] * pid(vrf_roll, FunctionsPitchRoll(AcX, AcY, AcZ), speedL, speedR, kp_roll, ki_roll, kd_roll, &e[0], &o[0]);
      pidval[1] = en[1] * pid(vrf_pitch, FunctionsPitchRoll(AcY, AcX, AcZ), speedFR, speedBK, kp_pitch, ki_pitch, kd_pitch, &e[1], &o[1]);
      //pidval[2] = en[2] * pid(vrf_yaw, FunctionsPitchRoll(AcY, AcX, AcZ), speedL, speedBK, kp_yaw, ki_yaw, kd_yaw, &e[2], &o[2]);
      speedFR = throttle + pidval[1] + pidval[2];
      speedBK = throttle - pidval[1] + pidval[2];
      speedL = throttle + pidval[0] - pidval[2];
      speedR = throttle - pidval[0] - pidval[2];

      if (speedFR > attmax) speedFR = attmax;
      if (speedFR < attmin) speedFR = attmin;
      if (speedBK > attmax) speedBK = attmax;
      if (speedBK < attmin) speedBK = attmin;
      if (speedL > attmax) speedL = attmax;
      if (speedL < attmin) speedL = attmin;
      if (speedR > attmax) speedR = attmax;
      if (speedR < attmin) speedR = attmin;
      motorFR.writeMicroseconds(speedFR);
      motorBK.writeMicroseconds(speedBK);
      motorL.writeMicroseconds(speedL);
      motorR.writeMicroseconds(speedR);
      /* t=millis()-t2;
        fun=f_yaw(0.3*t,1);
        speed=throttle+10*fun;

        motor.writeMicroseconds(speed);
        Serial.print(fun);
        Serial.print("  ");
        Serial.print(t);
        Serial.print("  ");*/
      Serial.print(speedFR);
      Serial.print(" ");
      Serial.print(speedBK);
      Serial.print(" ");
      Serial.print(pidval[1]);
      Serial.print(" ");
      Serial.print(o[1]);
      Serial.println(" ");
      go = Serial.read();
      if (go == 48) {
        motorFR.writeMicroseconds(1000);
        motorBK.writeMicroseconds(1000);
        motorL.writeMicroseconds(1000);
        motorR.writeMicroseconds(1000);
        for (i = 0; i < 3; i++) o[i] = 0;
        for (i = 0; i < 3; i++) e[i] = 0;
      }

    } while (go != 48);
  }

}

/*
   vref: vel_riferimento
   v: velocità
   att1: vel_mot1
   att2: vel_mot2
   kp
   ki
   kd
    errp: err_prima
    o: sommatoria integrale
*/
float pid(int vref, int v, int att1, int att2, float p, float i, float d, float *errp, float *o) {
  float der, err; //derivata errore, errore
  float pid;
  err = vref - v;

  der = err - *errp;
  if (att1 < MAX_THROTTLE && att1 > MIN_THROTTLE && att2 < MAX_THROTTLE && att2 > MIN_THROTTLE) *o = *o + err;

  pid = p * err + i * (*o) + d * der;
  *errp = err;

  return (int)pid;
}

double FunctionsPitchRoll(double A, double B, double C) {
  double DatoA, DatoB, Value;
  DatoA = A;
  DatoB = (B * B) + (C * C);
  DatoB = sqrt(DatoB);

  Value = atan2(DatoA, DatoB);
  Value = Value * 180 / 3.14;

  return (int)Value;
}
