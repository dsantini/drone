#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>
#include <Wire.h>

#define DEBUG_JOYSTICK
//#define DEBUG_GYRO

#define MPU 0x68

struct joystick {
  int throttle, pitch, roll, yaw;
};

struct angoli {
  double pitch, roll, yaw;
};

struct slaveData {
  joystick j;
  angoli a;
};

RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";
slaveData s = {{0, 0, 0, 0}, {0, 0, 0}};

#if defined(DEBUG_JOYSTICK) || defined(DEBUG_GYRO)
#define DEBUG
#endif

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
  Serial.println("\n###############     Caricamento in corso...     ###############");
  Serial.println("Setup I2C...");
#endif

  Wire.begin(); // join i2c bus (address optional for master)

  Wire.beginTransmission(MPU);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);

#ifdef DEBUG
  Serial.println("Setup radio...");
#endif

  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();

#ifdef DEBUG
  Serial.println(radio.isChipConnected() ? "Radio connesso" : "Radio non connesso");
  //radio.printDetails();
  Serial.println("###############     Caricamento completato     ###############");
#endif
}

void loop() {
  if (radio.available()) {
    radio.read(&s.j, sizeof(s.j));

#ifdef DEBUG_JOYSTICK
    Serial.print("Js Pitch: ");
    Serial.print(s.j.pitch);
    Serial.print("\t Js Roll: ");
    Serial.print(s.j.roll);
    Serial.print("\t Js Yaw: ");
    Serial.println(s.j.yaw);
#endif
  }

  s.a = leggiAccelerometro();

  Wire.beginTransmission(8); // transmit to device #8
  Wire.write((byte*)&s, sizeof(s));
  Wire.endTransmission();    // stop transmitting

  delay(10);
}

struct angoli leggiAccelerometro() {
  struct angoli a;
  int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;

  Wire.beginTransmission(MPU);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 14, true); // request a total of 14 registers

  AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp = Wire.read() << 8 | Wire.read(); // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX = Wire.read() << 8 | Wire.read(); // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY = Wire.read() << 8 | Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ = Wire.read() << 8 | Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  a.pitch  = FunctionsPitchRoll(AcY, AcX, AcZ);
  a.roll = FunctionsPitchRoll(AcX, AcY, AcZ);
  a.yaw = 0; // TODO

#ifdef DEBUG_GYRO
  Serial.print("Pitch: ");
  Serial.print(a.pitch);
  Serial.print("\t Roll: ");
  Serial.print(a.roll);
  Serial.print("\t Yaw: ");
  Serial.println(a.yaw);
#endif

  return a;
}



double FunctionsPitchRoll(double A, double B, double C) {
  double DatoA, DatoB, Value;
  DatoA = A;
  DatoB = (B * B) + (C * C);
  DatoB = sqrt(DatoB);

  Value = atan2(DatoA, DatoB);
  Value = Value * 180 / 3.14;

  return (int)Value;
}
