#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

struct joystick{
  int a;
  int b;
  };

RF24 radio(7,8); // CE, CSN
const byte address[6] = "00001";
int i;
joystick j;

void setup() {
  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
  i=0;
  j.a=1234;
  j.b=789;
}
void loop() {
  const char text[] = "Hello World";
  i++;
  //radio.write(&text, sizeof(text));
  radio.write(&j, sizeof(j));
  delay(200);
}
