#include <Servo.h>
#include <Wire.h>
#include <stdarg.h>
#include <stdio.h>

#define DEBUG_MOTORI
//#define DEBUG_I2C

#define FRONT_PIN 11
#define BACK_PIN 10
#define LEFT_PIN 9
#define RIGHT_PIN 8

#define KP 80
#define KI 0
#define KD 20

#define KP_ROLL KP
#define KI_ROLL KI
#define KD_ROLL KD

#define KP_PITCH KP
#define KI_PITCH KI
#define KD_PITCH KD

#define KP_YAW KP
#define KI_YAW KI
#define KD_YAW KD

#define MAX_THROTTLE 1930
#define MIN_THROTTLE 1070

#define CALIBRATION_THROTTLE 2000
#define CALIBRATION_TIME 15000 // millisecondi

#define ENABLE_ROLL 1
#define ENABLE_PITCH 1
#define ENABLE_YAW 0

struct joystick {
  int throttle, pitch, roll, yaw;
};

struct angoli {
  double pitch, roll, yaw;
};

struct slaveData {
  struct joystick j;
  struct angoli a;
};

Servo motorFR, motorBK, motorL, motorR;
const int MPU = 0x68; // I2C address of the MPU-6050
slaveData s = {{MIN_THROTTLE, 0, 0, 0}, {0, 0, 0}};
float speedFR = MIN_THROTTLE, speedBK = MIN_THROTTLE, speedL = MIN_THROTTLE, speedR = MIN_THROTTLE;
float errPitch = 0, errRoll = 0, errYaw = 0;
float sumPitch = 0, sumRoll = 0, sumYaw = 0;

#if defined(DEBUG_MOTORI) || defined(DEBUG_I2C)
#define DEBUG
#endif

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
  Serial.println("\n###############     Caricamento in corso...     ###############");
  Serial.println("Setup I2C...");
#endif

  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent); // register event

#ifdef DEBUG
  Serial.println("Setup motori...");
#endif

  motorFR.attach(FRONT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  motorBK.attach(BACK_PIN, MIN_THROTTLE, MAX_THROTTLE);
  motorL.attach(LEFT_PIN, MIN_THROTTLE, MAX_THROTTLE);
  motorR.attach(RIGHT_PIN, MIN_THROTTLE, MAX_THROTTLE);

  pinMode(A15, INPUT_PULLUP);
  if ( ! digitalRead(A15)) {
#ifdef DEBUG
    Serial.println("Calibrazione ESC...");
    Serial.print("Throttle: ");
    Serial.println(CALIBRATION_THROTTLE);
#endif

    motorFR.writeMicroseconds(CALIBRATION_THROTTLE);
    motorBK.writeMicroseconds(CALIBRATION_THROTTLE);
    motorL.writeMicroseconds(CALIBRATION_THROTTLE);
    motorR.writeMicroseconds(CALIBRATION_THROTTLE);

    delay(CALIBRATION_TIME);
  } else {
    motorFR.writeMicroseconds(speedFR);
    motorBK.writeMicroseconds(speedBK);
    motorL.writeMicroseconds(speedL);
    motorR.writeMicroseconds(speedR);
  }

#ifdef DEBUG
  Serial.println("###############     Caricamento completato     ###############");
#endif
}

void loop() {
  struct slaveData sd = s; // s può variare nel bel mezzo della funzione, creo un backup fisso

  float pidRoll = ENABLE_ROLL * pid(sd.j.roll, sd.a.roll, speedL, speedR, KP_ROLL, KI_ROLL, KD_ROLL, &errRoll, &sumRoll),
        pidPitch = ENABLE_PITCH * pid(sd.j.pitch, sd.a.pitch, speedFR, speedBK, KP_PITCH, KI_PITCH, KD_ROLL, &errPitch, &sumPitch),
        pidYaw = ENABLE_YAW * pid(sd.j.yaw, sd.a.yaw, speedL, speedBK, KP_YAW, KI_YAW, KD_YAW, &errYaw, &sumYaw);

  speedFR = constrain(sd.j.throttle + pidPitch + pidYaw, MIN_THROTTLE, MAX_THROTTLE);
  speedBK = constrain(sd.j.throttle - pidPitch + pidYaw, MIN_THROTTLE, MAX_THROTTLE);
  speedL = constrain(sd.j.throttle + pidRoll - pidYaw, MIN_THROTTLE, MAX_THROTTLE);
  speedR = constrain(sd.j.throttle - pidRoll - pidYaw, MIN_THROTTLE, MAX_THROTTLE);

#ifdef DEBUG_MOTORI
  Serial.print("Front: ");
  Serial.print(speedFR);
  Serial.print("\t Right: ");
  Serial.print(speedR);
  Serial.print("\t Back: ");
  Serial.print(speedBK);
  Serial.print("\t Left: ");
  Serial.println(speedL);
#endif

  motorFR.writeMicroseconds(speedFR);
  motorBK.writeMicroseconds(speedBK);
  motorL.writeMicroseconds(speedL);
  motorR.writeMicroseconds(speedR);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
  Wire.readBytes((byte*)&s, sizeof(s));

#ifdef DEBUG_I2C
  Serial.print("Js throttle: ");
  Serial.print(s.j.throttle);
  Serial.print("\t Js pitch: ");
  Serial.print(s.j.pitch);
  Serial.print("\t Js roll: ");
  Serial.print(s.j.roll);
  Serial.print("\t Roll: ");
  Serial.print(s.a.roll);
  Serial.print("\t Pitch: ");
  Serial.print(s.a.pitch);
  Serial.print("\t Yaw: ");
  Serial.println(s.a.yaw);
#endif
}

/*
   vref: vel_riferimento
   v: velocità
   att1: vel_mot1
   att2: vel_mot2
   kp
   ki
   kd
    errp: err_prima
    o: sommatoria integrale
*/
float pid(int vref, int v, int att1, int att2, float p, float i, float d, float *errp, float *o) {
  float der, err; //derivata errore, errore
  float pid;
  err = vref - v;

  der = err - *errp;
  if (att1 < MAX_THROTTLE && att1 > MIN_THROTTLE && att2 < MAX_THROTTLE && att2 > MIN_THROTTLE) *o = *o + err;

  pid = p * err + i * (*o) + d * der;
  *errp = err;

  return (int)pid;
}
